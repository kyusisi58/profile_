package com.aplayz.player.controller;

import com.aplayz.player.model.BrandMusicInfo;
import com.aplayz.player.model.LikeMusic;
import com.aplayz.player.service.PlayerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(path = {"/player"})
public class PlayerController {

    private final PlayerService playerService;

    //장르 리스트 가져오기
    @PostMapping("/getMySpaceInfo")
    public ResponseEntity<?> getMySpaceInfo() {
        return null;
    }

    /**
     * IF_APLAYZ_PALY-MYSPACEWEATHER-API-002
     * 기상청 API 리턴 -> 확인 해야함
     *
     * API : /getMySpaceWeather
     */


    /**
     * TODO: 큐레이션 API
     * IF_APLAYZ_PALY-MUSICLIST-API-003 : 큐레이션 음악 리스트 회신
     * IF_APLAYZ_PALY-ANOTHERMUSICLIST-API-006 : 다음 분위기 곡 리스트 가져오기
     *
     * API : /getMusicList
     */
    @PostMapping("/getMusicList")
    public ResponseEntity<?> getMusicList() {
        return ResponseEntity.status(HttpStatus.OK).body(playerService.getMusicList());
    }


    /**
     * IF_APLAYZ_PALY-LIKEMUSIC-API-004
     * 플레이어 음악 like/dislike 설정
     *
     * API : /setLikeMusic
     * @return
     */
    //장르 리스트 가져오기
    @PostMapping("/setLikeMusic")
    public int setLikeMusic(@RequestBody LikeMusic likeMusic) {
        try {
            return playerService.setLikeMusic(likeMusic);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return 0;
        }
    }

    /**
     * IF_APLAYZ_PALY-BRANDMUSICLIST-API-007
     * 플레이어 브랜드 음악 리스트 가져오기
     *
     */
    @PostMapping("/getBrandMusicList")
    public ResponseEntity<?> getBrandMusicList(@RequestBody BrandMusicInfo brandMusicInfo) {
        return ResponseEntity.status(HttpStatus.OK).body(playerService.getBrandMusicList(brandMusicInfo));
    }

    /**
     * IF_APLAYZ_PALY-BRANDMUSIC-API-008
     * 해당 매장에 연결된 브랜드의 음악 설정
     *
     */
    @PostMapping("/setBrandMusic")
    public int setBrandMusic(@RequestBody BrandMusicInfo brandMusicInfo) {
        try {
            return playerService.setBrandMusic(brandMusicInfo);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return 0;
        }
    }
}

