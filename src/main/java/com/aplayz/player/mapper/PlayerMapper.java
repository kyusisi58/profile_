package com.aplayz.player.mapper;

import com.aplayz.player.model.BrandMusicInfo;
import com.aplayz.player.model.LikeMusic;
import com.aplayz.player.model.MusicInfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.ArrayList;

@Mapper
public interface PlayerMapper {
    /**
     * 플레이어에서 재생중인 음악에 대한 좋아요 선택/해제하기
     * @param likeMusic
     * @return
     */
    int setLikeMusic(LikeMusic likeMusic);

    /**
     *
     * @param brandMusicInfo
     * @return
     */
    int setBrandMusic(BrandMusicInfo brandMusicInfo);

    /**
     *
     * @return
     */
    ArrayList<MusicInfo> selectMusicList();

    /**
     *
     * @param brandMusicInfo
     * @return
     */
    ArrayList<BrandMusicInfo> selectBrandMusicList(BrandMusicInfo brandMusicInfo);

}
