package com.aplayz.player.service;

import com.aplayz.player.mapper.PlayerMapper;
import com.aplayz.player.model.BrandMusicInfo;
import com.aplayz.player.model.LikeMusic;
import com.aplayz.player.model.MusicInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;

@Service
@Slf4j
@RequiredArgsConstructor
public class PlayerService {
    private final PlayerMapper playMapper;

    public ArrayList<MusicInfo> getMusicList() {
        /**
         * TODO: 큐레이션 API 사용 전 임시 데이터 리턴
         */
        ArrayList<MusicInfo> musicList = playMapper.selectMusicList();
        log.info("info => {}", musicList);
        return musicList;
    }

    public ArrayList<BrandMusicInfo> getBrandMusicList(BrandMusicInfo brandMusicInfo) {
        ArrayList<BrandMusicInfo> brandMusicInfoList = playMapper.selectBrandMusicList(brandMusicInfo);
        log.info("info => {}", brandMusicInfoList);
        return brandMusicInfoList;
    }

    public int setLikeMusic(LikeMusic likeMusic) {
        int result = 0;

        if ("Y".equals(likeMusic.getLike())) {
            // 좋아요 일때
            likeMusic.setUnlike(null);
            result = playMapper.setLikeMusic(likeMusic);
        }else {
            // 싫어요 일때
            likeMusic.setLikeId(null);
            result = playMapper.setLikeMusic(likeMusic);
        }

        return result;
    }

    public int setBrandMusic(BrandMusicInfo brandMusicInfo) {
        int result = 0;
        /**
         * TODO: tb_brand_space_music 테이블 생성 이후 작업 시작.
         */

        return 1;
    }
}
