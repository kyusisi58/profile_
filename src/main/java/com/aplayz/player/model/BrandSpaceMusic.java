package com.aplayz.player.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class BrandSpaceMusic {
    private String brandSpaceMusicId;
    private String  nbrand_music_id;
    private String spaceId;
    private String selectTime;
    private String repeatTime;
    private String playAt;
    private Date registDate;
    private Date updateDate;
}
