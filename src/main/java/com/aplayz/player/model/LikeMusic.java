package com.aplayz.player.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class LikeMusic {
	private String likeId;
	private String musicId;
	private String spaceId;
	private String like;
	private String unlike;
	private Date registDate;
	private Date updateDate;
}
