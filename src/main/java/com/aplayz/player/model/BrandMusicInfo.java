package com.aplayz.player.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class BrandMusicInfo {
    private String brandMusicId;
    private String brandId;
    private String spaceId;
    private String brandMusicNm;
    private Date playtime;
    private String useAt;
    private String brandMusicPath;
    private String brandMusicImg;
    private Date registDate;
    private Date updateDate;
}
