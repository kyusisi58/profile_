package com.aplayz.player.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MusicInfo {
    private String musicId;
    private String artist;
    private String musicNm;
    private String genre;
    private String album;
    private Date playtime;
    private String tune;
    private Integer modern;
    private Integer luxury;
    private Integer vintage;
    private Integer cute;
    private Integer hip;
    private Integer cafe;
    private Integer food;
    private Integer pub;
    private Integer hospital;
    private Integer convenience;
    private Integer edu;
    private Integer inn;
    private Integer morning;
    private Integer afternoon;
    private Integer evening;
    private Integer dawn;
    private Integer spring;
    private Integer summer;
    private Integer fall;
    private Integer winter;
    private Integer sunny;
    private Integer cloudy;
    private Integer rain;
    private Integer heavyrain;
    private Integer snow;
    private Integer calm;
    private Integer happy;
    private Integer sad;
    private Integer upset;
    private Integer fantasy;
    private String musicPath;
    private String albumImg;
    private Integer playCount;
    private Date registDate;
    private Date updateDate;

}
